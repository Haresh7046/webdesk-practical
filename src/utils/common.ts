const generator = require('generate-password');
const nodemailer = require('nodemailer');
const fs = require("fs");
const handlebars = require('handlebars');

export const generateStrongPassword = () => {
    return generator.generate({
        length: 16,
        numbers: true,
        symbols: true,
        lowercase: true,
        uppercase: true,
        excludeSimilarCharacters: true,
        exclude: ""
    });
};
export const generateRandomCode = () => {
    return generator.generate({
        length: 6,
        numbers: true,
        symbols: false,
        lowercase: false,
        uppercase: false,
        excludeSimilarCharacters: false
    });
};

export async function sendEmail(email_params: any = []) {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    // const account = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: process.env.MAIL_SECURE, // true for 465, false for other ports
        secureConnection: process.env.MAIL_SECURE,
        service: 'gmail',
        auth: {
            user: process.env.MAIL_USERNAME, // generated ethereal user
            pass: process.env.MAIL_PASSWORD // generated ethereal password
        },
        tls: {
            ciphers: 'SSLv3'
        },
        logger: false,
        debug: false
    });

    // send mail with defined transport object
    const mailOptions = {
        from: '"Haresh Makwana <' + process.env.MAIL_FROM + '>', // sender address
        to: email_params.email, // list of receivers
        cc: (email_params.cc) ? email_params.cc : [],
        subject: email_params.subject, // Subject line
        text: email_params.body, // plain text body
        html: `${email_params.html}`,
        attachments: email_params.attachments
    };

    const info = await transporter.sendMail(mailOptions, (error) => {
        if (error) {
            console.log('Error occurred');
            console.log(error.message);
            return error;
        }
        console.log('Message sent successfully!');
        // only needed when using pooled connections
        transporter.close();
    });
    return info;
}

export async function generateDynamicHtml(path, data, html = null) {
    try{
        let html_format = '';
        if(path){
            html_format = await fs.readFileSync(path, 'utf8');
        } else {
            html_format = html;
        }
        let template = handlebars.compile(html_format);
        let quotation = {};
        quotation = Object.assign(quotation, data);
        return template(quotation);
    } catch (error){
        console.log(error);
        return error.message;
    }
}
