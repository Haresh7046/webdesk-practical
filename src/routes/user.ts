import { Router } from "express";
import UserController from "../controller/UserController";
import { checkJwt } from "../middlewares/checkJwt";
import imageUpload from "../middlewares/fileUpload";

const router = Router();

//Get all users
router.get("/", [checkJwt], UserController.listAll);

// Get one user
router.get(
    "/:id([0-9]+)",
    [checkJwt],
    UserController.getOneById
);

//Create a new user
router.post("/", [imageUpload], UserController.newUser);

//Edit one user
router.put(
    "/:id([0-9]+)",
    [checkJwt, imageUpload],
    UserController.editUser
);

//Delete one user
router.delete(
    "/:id([0-9]+)",
    [checkJwt],
    UserController.deleteUser
);

export default router;
