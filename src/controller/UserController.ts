import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";

import {User} from "../entity/User";
import {generateStrongPassword, generateRandomCode, generateDynamicHtml, sendEmail} from "../utils/common";

class UserController {

    static listAll = async (req: Request, res: Response) => {
        //Get users from database
        const userRepository = getRepository(User);
        const users = await userRepository.find({
            select: ["id", "username", "dob", "email", "gender", "profileImageUrl"]  //We dont want to send the passwords on response
        });

        //Send the users object
        res.status(200).send({
            status: "success",
            message: "User list retrieve successfully",
            data: users
        });
    };

    static getOneById = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        //Get the user from database
        const userRepository = getRepository(User);
        try {
            const user = await userRepository.findOneOrFail(id, {
                select: ["id", "username", "dob", "email", "gender", "profileImageUrl"] //We dont want to send the password on response
            });
            res.status(200).send({
                status: "success",
                message: "User detail retrieve successfully",
                data: [user]
            });
        } catch (error) {
            res.status(404).send(
                {
                    status: "error",
                    message: "User not found",
                    data: []
                });
        }
    };

    static newUser = async (req: Request, res: Response) => {
        //Get parameters from the body
        const {username, email, mobile, gender, dob} = req.body;
        const password = generateStrongPassword();
        let user = new User();
        user.username = username;
        user.email = email;
        user.mobile = mobile;
        user.gender = gender;
        user.dob = dob;
        user.password = password;
        user.emailCode = generateRandomCode();
        user.profileImageUrl = req.file ? req.file.filename: null;

        // Validate if the parameters are ok
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }
        //Hash the password, to securely store on DB
        user.hashPassword();

        //Try to save. If fails, the username is already in use
        const userRepository = getRepository(User);
        try {
            await userRepository.save(user);
        } catch (e) {
            res.status(409).send({
                message: "Email or Mobile already in use",
                status: "error"
            });
            return;
        }

        user.password = password;

        const html = await generateDynamicHtml('./views/email/userDetail.html', user);
        const emailParams = {
            subject: "Welcome to Webdesk Solution",
            email: user.email,
            body: html,
            html: `${html}`,
        };
        setTimeout(() => {
            sendEmail(emailParams);
        }, 2000);

        delete user.password;
        delete user.emailCode;

        //If all ok, send 201 response
        res.status(201).send({
                message: "User created successfully",
                status: "success",
                data: [user]
            }
        );
    };

    static editUser = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = Number(req.params.id);

        //Try to find user on database
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send({
                message: "User not found",
                status: "error"
            });
            return;
        }
        user = Object.assign(user, req.body);
        user.id = id;

        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await userRepository.save(user);
        } catch (e) {
            console.log({e});
            res.status(409).send({
                message: "email or mobile already in use",
                status: "error"
            });
        }
        //After all send a 204 (no content, but accepted) response
        res.status(200).send({
            message: "User updated successfully",
            status: "success"
        });
    };

    static deleteUser = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        const userRepository = getRepository(User);
        try {
            await userRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send({
                message: "User not found",
                status: "error"
            });
            return;
        }
        await userRepository.softDelete(id);

        //After all send a 204 (no content, but accepted) response
        res.status(200).send({
            message: "User deleted successfully",
            status: "success"
        });
    };
};

export default UserController;
