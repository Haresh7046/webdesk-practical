import {getRepository} from "typeorm";
import {User} from "../entity/User";

class UserRepository{

    async getUserList() {
        const userRepository = getRepository(User);
        return await userRepository.find({
            select: ["id", "username", "dob", "email", "gender", "profileImageUrl"]  //We dont want to send the passwords on response
        });
    }
    async getUser(id: number, select: any[]) {
        const userRepository = getRepository(User);
        return await userRepository.findOneOrFail(id, {
            select: ["id", "username", "dob", "email", "gender", "profileImageUrl"] //We dont want to send the password on response
        });
    }
}

export default UserRepository;
