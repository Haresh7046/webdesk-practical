import config from "../config/config";
const multer = require('multer');
const path = require('path');

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.filePath)
    },
    filename: (req, file, cb) => {
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/i)){
            return cb(new Error('Please upload valid image'));
        }
        let ext = path.extname(file.originalname);
        cb(null, file.fieldname + '-' + Date.now()+ext)
    },
    limits: {
        fileSize: 1000000
    }
});

const imageUpload = multer({storage: fileStorage}).single('profileImageUrl');

export default imageUpload;
