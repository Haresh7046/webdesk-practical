import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn, DeleteDateColumn
} from "typeorm";
import {Length, IsNotEmpty, IsEmail, IsDate, IsNumber, IsMobilePhone} from "class-validator";
import * as bcrypt from "bcryptjs";

enum Gender {
    Male = 'male',
    Female = 'female',
    Other = 'other'
}

@Entity()
@Unique(["email", "mobile"])
export class User {
    @PrimaryGeneratedColumn({type: "int"})
    id: number;

    @Column()
    @Length(4, 20)
    username: string;

    @Column()
    @Length(4, 100)
    password: string;

    @Column()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @Column()
    @IsNotEmpty()
    @Length(10)
    @IsMobilePhone()
    mobile: number;

    @Column()
    @IsNotEmpty()
    gender: Gender;

    @Column({type: 'date'})
    @IsNotEmpty()
    dob: Date;

    @Column()
    @IsNotEmpty()
    profileImageUrl: string;

    @Column()
    @IsNotEmpty()
    emailCode: string;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8);
    }

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }
}
